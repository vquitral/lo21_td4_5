
#ifndef _TRADING_H
#define _TRADING_H

#include <string>

using namespace std;

class TradingException{
    public:
        TradingException(const string& message) :info(message) {}
        string getInfo() const { return info; }
    private:
        string info;
};

class Devise{
    private :
        char code[4];
        string monnaie, zone;
    public:
        Devise(const string& c, const string& m, const string& z="");
        string getCode() const { return code; }
        const string& getMonnaie() const { return monnaie; }
        const string& getZoneGeographique() const { return zone; }
};

class PaireDevises{
    private :
        const Devise* base;
        const Devise* contrepartie;
        string surnom;
    public:
        PaireDevises(const Devise& b, const Devise& c, const string& s = "");
        const Devise& getBase() const { return *base; }
        const Devise& getContrepartie() const { return *contrepartie; }
        const string& getSurnom() const { return surnom; }
};

class CoursOHLC{
    private :
        double open,high,low,close;
    public:
        CoursOHLC(double o=0, double h=0, double l=0, double c=0);
        double getOpen() const { return open; }
        double getHigh() const { return high; }
        double getLow() const { return low; }
        double getClose() const { return close; }
        void setCours(double o, double h, double l, double c);
};

ostream& operator<<(ostream& f, const Devise& d);
ostream& operator<<(ostream& f, const PaireDevises& p);
ostream& operator<<(ostream& f, const CoursOHLC& c);

class EvolutionCours{
    private :
        const PaireDevises &paire;
        unsigned int nbMaxCours;
        unsigned int nbCours;
        CoursOHLC *cours;

    public :
        EvolutionCours(const PaireDevises& p, unsigned int n);
        void addCours(double o, double h, double l, double c);
        ~EvolutionCours();
        EvolutionCours &operator=(const EvolutionCours &EC);
        EvolutionCours(EvolutionCours const & EC);

};
#endif
