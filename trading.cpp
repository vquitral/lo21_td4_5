
#include "trading.h"
#include <ostream>

Devise::Devise(const string& c, const string& m, const string& z): monnaie(m), zone(z) {
    if (c.size() != 3) throw TradingException("code devise incorrect");
    for (unsigned int i = 0; i < 3; i++) {
        if (c[i] <= 'A' || c[i] >= 'Z') throw TradingException("code devise incorrect");
        code[i] = c[i];
    }
    code[3] = '\0';
}

PaireDevises::PaireDevises(const Devise& b, const Devise& c, const string& s): base(&b), contrepartie(&c), surnom(s) {}

CoursOHLC::CoursOHLC(double o, double h, double l, double c): open(o), high(h), low(l), close(c) {
    if (o<0||h<0||l<0||c<0||l>h) throw TradingException("cours OHLC incorrect");
}

void CoursOHLC::setCours(double o, double h, double l, double c) {
    if (o < 0 || h < 0 || l < 0 || c<0 || l>h) throw TradingException("cours OHLCincorrect");
    open = o; high = h; low = l; close = c;
}
ostream& operator<<(ostream& f, const Devise& d) {
    f <<d.getCode()<<" ("<<d.getMonnaie()<<", "<< d.getZoneGeographique()<<")";
    return f;
}
ostream& operator<<(ostream& f, const PaireDevises& p) {
    f << p.getBase().getCode() << "/" << p.getContrepartie().getCode();
    return f;
}
ostream& operator<<(ostream& f, const CoursOHLC& c) {
    f<<"["<<c.getOpen()<<" ("<< c.getHigh()<<", " <<c.getLow()<<") "<<c.getClose()<<"]";
    return f;
}

EvolutionCours::EvolutionCours(const PaireDevises& p, unsigned int n) : paire(p), nbMaxCours(n), nbCours(0) , cours(0){
  CoursOHLC *newTab = new CoursOHLC[n];
  cours = newTab;

}

void EvolutionCours::addCours(double o, double h, double l, double c){
    CoursOHLC nouveau(o,h,l,c);
    if (nbCours == nbMaxCours){
        CoursOHLC *newTabCours = new CoursOHLC[nbMaxCours+100];
        for(unsigned int i(0); i < nbCours; i++){
            newTabCours[i] = cours[i];
        }
        CoursOHLC *old = cours;
        cours = newTabCours;
        nbMaxCours += 100;
        delete []old;
    }
    cours[nbCours] = nouveau;
    nbCours++;
}

EvolutionCours::~EvolutionCours(){
    delete[] cours;
}

EvolutionCours &EvolutionCours::operator=(const EvolutionCours &EC){
    if (this != &EC){
        cours = 0;
        nbMaxCours = EC.nbMaxCours;
        nbCours = EC.nbCours;
        paire = EC.paire;
    }
    return *this;
}

EvolutionCours::EvolutionCours(EvolutionCours const & EC) : paire(EC.paire), nbMaxCours(EC.nbMaxCours), nbCours(EC.nbCours), cours(0){
    CoursOHLC *newTabCours = new CoursOHLC[nbMaxCours];
    for (unsigned int i(0); i < nbMaxCours ; i++){
        newTabCours[i] = EC.cours[i];
    }
    cours = newTabCours;
}

